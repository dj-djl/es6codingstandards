const x = {
  bad: 'indent',
};

const y = z => 
{ 
  console.log('whatever', z); 
};

const arr = [1, 2, 3];
const arr2 = 
[
  1, 2, 3,
];

const Myfunc = function Myfunc(p)
{
  if (p <= 0) { return; }
  Myfunc(p - 1);
};

console.log(x, y, arr, arr2, Myfunc());